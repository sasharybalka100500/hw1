const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');

app.use(express.json());

let createFile = (req, res) => {
  if (!req.body) {
    return res
      .status(400)
      .send({ message: "Please specify 'content' parameter" });
  }
  const dir = './files';

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  let isSupportedFile = (file) => {
    if (
      file.includes('.txt') ||
      file.includes('.log') ||
      file.includes('.json') ||
      file.includes('.yaml') ||
      file.includes('.xml') ||
      file.includes('.js')
    ) {
      return true;
    } else {
      return false;
    }
  };
  if (!req.body.content) {
    res.status(400).send({ message: "Please specify 'content' parameter" });
  } else if (!req.body.filename) {
    res.status(400).send({ message: "Please specify 'filename' parameter" });
  } else if (!isSupportedFile(req.body.filename)) {
    res.status(400).send({ message: 'Not supported file extension' });
  } else if (req.body.filename && req.body.content) {
    fs.writeFile(
      `./files/${req.body.filename}`,
      `${req.body.content}`,
      (err) => {
        res.status(200).send({ message: 'File created successfully' });
      }
    );
  } else {
    res.status(500).send({ message: 'Server error' });
  }
};
app.post('/api/files', createFile);

let getFiles = (req, res) => {
  try {
    const testFolder = './files/';
    fs.readdir(testFolder, (err, files) => {
      if (err) {
        res.status(400).send({ message: 'Client error' });
      } else {
        res.status(200).send({ message: 'Success', files: files });
      }
    });
  } catch (error) {
    res.status(500).send({ message: 'Server error' });
  }
};
app.get('/api/files', getFiles);

let getFile = (req, res) => {
  try {
    let filename = req.params.filename;
    let extension = path.extname(filename);
    let createDate = '';
    fs.stat(`./files/${filename}`, (error, stats) => {
      if (error) {
        console.log(error);
        return;
      }
      createDate = stats.birthtime;
    });
    fs.readFile(`./files/${filename}`, 'utf8', (err, data) => {
      if (err) {
        console.log(`No file with ${filename} filename found`);
        res.status(400).send({
          message: `No file with ${filename} filename found`,
        });
      } else {
        res.status(200).send({
          message: 'Success',
          filename: filename,
          content: data,
          extension: extension.slice(1),
          uploadedDate: createDate,
        });
      }
    });
  } catch (error) {
    res.status(500).send({ message: 'Server error' });
  }
};

app.get(`/api/files/:filename`, getFile);

app.listen(8080, () => {
  console.log('Started on PORT 8080');
});
